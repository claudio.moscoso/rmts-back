package cl.buildersoft.rmts.config;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

	@Autowired
	@Qualifier("TokenEnhancer")
	TokenEnhancer tokenEnhancer;

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	@Autowired
	@Qualifier("authenticationManager")
	private AuthenticationManager authenticationManager;

	@Override
	public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
		security//
				.tokenKeyAccess("permitAll()")//
				.checkTokenAccess("isAuthenticated()");
	}

	@Override
	public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
		clients//
				.inMemory()//
				.withClient("rmts-frnt")//
				.secret(passwordEncoder.encode("alguna-clave"))//
				.scopes("read", "write")//
				.authorizedGrantTypes("password", "refresh_token")//
				.accessTokenValiditySeconds(3600)//
				.refreshTokenValiditySeconds(3600);
	}

	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
		TokenEnhancerChain tec = new TokenEnhancerChain();
		tec.setTokenEnhancers(Arrays.asList(tokenEnhancer, accessTokenConverter()));

		endpoints//
				.authenticationManager(authenticationManager)//
				.tokenStore(tokenStore())//
				.accessTokenConverter(accessTokenConverter())//
				.tokenEnhancer(tec);
	}

	@Bean
	public JwtTokenStore tokenStore() {
		return new JwtTokenStore(accessTokenConverter());
	}

	@Bean
	public JwtAccessTokenConverter accessTokenConverter() {
		JwtAccessTokenConverter jwtAccessTokenConverter = new JwtAccessTokenConverter();
		jwtAccessTokenConverter.setSigningKey(SecurityConsts.STRING_KEY);
//		jwtAccessTokenConverter.setSigningKey(SecurityConsts.PRIVATE_KEY);
//		jwtAccessTokenConverter.setVerifierKey(SecurityConsts.PUBLIC_KEY);
		return jwtAccessTokenConverter;
	}

}
