package cl.buildersoft.rmts.config.impl;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.stereotype.Component;

import cl.buildersoft.rmts.user.application.UserService;

@Component("TokenEnhancer")
public class TokenEnhancerImpl implements TokenEnhancer {
	Logger LOG = LoggerFactory.getLogger(TokenEnhancerImpl.class);

	@Autowired
	UserService userService;

	@Override
	public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
//		LOG.debug("In enhance {}", accessToken.getClass().getName());
//		UserEntity userEntity = userService.findByUsername(authentication.getName());

//		LOG.debug(accessToken.getAdditionalInformation().toString());

		Map<String, Object> info = new HashMap<String, Object>();
//		Map<String, Object> info = accessToken.getAdditionalInformation();

		info.put("info_aditional", "Saludos"); // .concat(authentication.getName()));
//		info.put("info_user", String.format("id: %s / name: %s", userEntity.getId(), userEntity.getUsername()));

		((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(info);
//		LOG.debug(accessToken.getAdditionalInformation().toString());

		return accessToken;
	}

}
