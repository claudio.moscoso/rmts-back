package cl.buildersoft.rmts.config;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {
	@Value("${cl.buildersoft.rmts.secure.disable}")
	boolean secureDisable;

	@Override
	public void configure(HttpSecurity http) throws Exception {
		ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry authorizeRequests = http//
				.authorizeRequests();

		if (secureDisable) {
			authorizeRequests = authorizeRequests.antMatchers("/**").permitAll();
		}
		authorizeRequests//

//		.antMatchers(HttpMethod.GET,"/**").permitAll()//
//				.antMatchers(HttpMethod.GET, "/api/product/1.0").permitAll()//
				.antMatchers(HttpMethod.GET, "/api/product/1.0/photo/{productId}").permitAll()//

				/**
				 * <code>
				.antMatchers(HttpMethod.GET, "/api/product/1.0/{productId}").hasAnyRole("ROLE_ADMIN", "ROLE_USER")//
				.antMatchers("/api/product/1.0").hasRole("ADMIN")
				</code>
				 */

				.anyRequest().authenticated()//
				.and()//
				.cors().configurationSource(corsConfigurationSource())//

		;
	}

	@Bean
	public CorsConfigurationSource corsConfigurationSource() {
		CorsConfiguration configuration = new CorsConfiguration();

		configuration.setAllowedOrigins(Arrays.asList("http://localhost:4200"));
		configuration.setAllowedMethods(Arrays.asList("*"));
//		configuration.setAllowedMethods(Arrays.asList("PUT", "GET", "POST", "OPTION", "DELETE"));
		configuration.setAllowCredentials(true);
		configuration.setAllowedHeaders(Arrays.asList("Content-Type", "Authorization"));
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();

		source.registerCorsConfiguration("/**", configuration);
		return source;

	}

	@Bean
	public FilterRegistrationBean<CorsFilter> corsFilter() {
		FilterRegistrationBean<CorsFilter> out = new FilterRegistrationBean<CorsFilter>(
				new CorsFilter(corsConfigurationSource()));
		out.setOrder(Ordered.HIGHEST_PRECEDENCE);
		return out;
	}

}
