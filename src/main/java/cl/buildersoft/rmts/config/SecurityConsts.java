package cl.buildersoft.rmts.config;

public class SecurityConsts {
	public static final String STRING_KEY = "este.es.una,cadena.cualquiera";
	public static final String PUBLIC_KEY = "-----BEGIN PUBLIC KEY-----\n"
			+ "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAsLgAvdZJ8NhM67okM7k5\n"
			+ "BEBzYWovPS3yyhQA528RrymHPUuOVH0wo4jgtJVOErldQP5weIXYiztbUWJR3zIn\n"
			+ "eYsE8BRvtlG1YRGewoaWqDKPCMb+CcZyObGFkJMWIP7dYj1l8TPz9E6q0RQcgANQ\n"
			+ "DxDYEBe11MToaLxH9n2srEcGjXNX1aWrsf3Vxj5Jyl/FcmWIXnR8B7fCMpcLk2ue\n"
			+ "LVE8UCOBybs5/+diRI0ruecTQ+XmNBSvxTAJkVvAfxOZ0uAEVRTMSahTNFRYmKQJ\n"
			+ "5TZxBi61PF0U5nFchgNpyP18X7tz8lwWhV5mbDkqjgES3NdxqSu4Uj5yy+gRcgxn\n" + "uQIDAQAB\n"
			+ "-----END PUBLIC KEY-----";
	public static final String PRIVATE_KEY = "-----BEGIN RSA PRIVATE KEY-----\n"
			+ "MIIEowIBAAKCAQEAsLgAvdZJ8NhM67okM7k5BEBzYWovPS3yyhQA528RrymHPUuO\n"
			+ "VH0wo4jgtJVOErldQP5weIXYiztbUWJR3zIneYsE8BRvtlG1YRGewoaWqDKPCMb+\n"
			+ "CcZyObGFkJMWIP7dYj1l8TPz9E6q0RQcgANQDxDYEBe11MToaLxH9n2srEcGjXNX\n"
			+ "1aWrsf3Vxj5Jyl/FcmWIXnR8B7fCMpcLk2ueLVE8UCOBybs5/+diRI0ruecTQ+Xm\n"
			+ "NBSvxTAJkVvAfxOZ0uAEVRTMSahTNFRYmKQJ5TZxBi61PF0U5nFchgNpyP18X7tz\n"
			+ "8lwWhV5mbDkqjgES3NdxqSu4Uj5yy+gRcgxnuQIDAQABAoIBAE+xCpSROBLmV/nN\n"
			+ "nVm941ppfh6l9duPBvfeAXJcggsNnTElC3BKbE6zATIuSmDjOox+sVWIdliDUfDn\n"
			+ "d6hBQ8/pwzZI4P/E5Rh7C68JZpM/CYWZxyG9XfFqfqLt0YLbrBYLPheBqDe56goT\n"
			+ "Ga8Yz1eb7vOC3bqveWcR0Rn1HpMpTImDDCr5PVNNj0RGQcLZLZjGQToTE1ru7bE+\n"
			+ "EdJxJ7JjpFCYg1gJeAffggu5/soyOtQPrV9/wsdXnTDe2+I7Zzjbbm1/ZCm3Pd4K\n"
			+ "+6iyBLE+j+A6ldXp5rMwm06SqVvlH/KdVomljXw1DGd8cW0lxYsVBQ+2h4xP5UJ2\n"
			+ "rBAA2CECgYEA2QN1DSJqtHILNg+RUHgjAVlCfeRT9yIz7mR7AIiE1IZurbZ6NUzx\n"
			+ "6lfCpHDyPq3BV2YF5JBhyQE9c07MSmOpExrjdNz7oyD7kUjRn7rA6FLDsFuGl3SG\n"
			+ "8oV4h7iOIsgSlusGddIRla9tl5ciGOPPnvmx3A4ZboaLzSANHfWGOmUCgYEA0Hdf\n"
			+ "ll9D1U6UPJGJY8BtuGwNSB0gxEBKmZyQeFG/xbkmg4uarXsiLw7unxC983kbn/1Q\n"
			+ "aS0WX+vk06//P33gNuLteitsQCxV3OH1JrSAoYSPiScpcOpL7wU2LAmBDvR1Du6s\n"
			+ "+pnNZTA2aU7NgKFHNLc+C4IPDhbSCtd1/5KoGMUCgYEAk+BpOKQFNiSoQxRknwLe\n"
			+ "R6L9Ti/yj2q9HGtX7csZSIC6lqDLsdpW1qsqtUdJqzBXmSiqR0+jcobnytUdHGvA\n"
			+ "4IXoDNAwoL5u7ldCkKd/oFAVjO0xT69V7ZjJwyIdqNHQ6QoKAus8lFVQRoXz9CX4\n"
			+ "aooXiq4I4mnIheSbS5js+IkCgYABxZH1aDfeXz2ZSi9dvqvwDlMKktzNbjJ0HcWI\n"
			+ "kEFN/+XY5l3ZLZu7xKe4iF7k69mVQYTmb/CpvgqC1hMYSgyWytTz3dycqwPLzcvU\n"
			+ "mWIifUrvAnsyr3sXu/aa5zXGl8NK7jJgKuB5BLDhsj7C4hwF0azgUimol/8PAofJ\n"
			+ "NUjidQKBgHK9ACP5pQ4Z5Vog0ThKUTmnodKrXkmIS4t7aA7vouW+YpyaihO9JhL/\n"
			+ "xkDqgGf14rYsxpqzKXPfPFKwAcDOFW8vRPlJEjcCwkAMqIDXd/913bZ+WFDtyjRF\n"
			+ "dxudT+sDbul3OX550hSq3ZkPCqYO/j4Oh6c7flwA63YRtGxS1VJV\n" + "-----END RSA PRIVATE KEY-----";
}
