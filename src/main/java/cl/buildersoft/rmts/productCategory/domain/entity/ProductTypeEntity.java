package cl.buildersoft.rmts.productCategory.domain.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "product_type")
public class ProductTypeEntity implements Serializable {
	private static final long serialVersionUID = 8139049106378299986L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(length = 50)
	private String name;

	@ManyToOne(fetch = FetchType.LAZY)
	// @JoinColumn(name="product_family_id")
	private ProductFamilyEntity productFamily;

	public ProductTypeEntity() {
	}

	public ProductTypeEntity(Integer id, String name, ProductFamilyEntity productFamily) {
		super();
		this.id = id;
		this.name = name;
		this.productFamily = productFamily;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ProductFamilyEntity getProductFamily() {
		return productFamily;
	}

	public void setProductFamily(ProductFamilyEntity productFamily) {
		this.productFamily = productFamily;
	}

	@Override
	public String toString() {
		return "ProductTypeEntity [id=" + id + ", name=" + name + ", productFamily=" + productFamily + "]";
	}

}
