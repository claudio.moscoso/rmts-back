package cl.buildersoft.rmts.productCategory.domain.dto;

public class ProductFamilyDTO {
	private Integer id;
	private String name;

	public ProductFamilyDTO() {
	}

	public ProductFamilyDTO(Integer id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "ProductFamilyDTO [id=" + id + ", name=" + name + "]";
	}

}
