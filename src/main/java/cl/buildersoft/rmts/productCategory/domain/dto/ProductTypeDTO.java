package cl.buildersoft.rmts.productCategory.domain.dto;

import java.io.Serializable;

public class ProductTypeDTO implements Serializable {
	private static final long serialVersionUID = -6838474873634528804L;

	private Integer id;

	private String name;

	private Integer productFamilyId;
	private String productFamilyName;

	public ProductTypeDTO() {
	}

	public ProductTypeDTO(Integer id, String name, Integer productFamilyId, String productFamilyName) {
		super();
		this.id = id;
		this.name = name;
		this.productFamilyId = productFamilyId;
		this.productFamilyName = productFamilyName;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getProductFamilyId() {
		return productFamilyId;
	}

	public void setProductFamilyId(Integer productFamilyId) {
		this.productFamilyId = productFamilyId;
	}

	public String getProductFamilyName() {
		return productFamilyName;
	}

	public void setProductFamilyName(String productFamilyName) {
		this.productFamilyName = productFamilyName;
	}

	@Override
	public String toString() {
		return "ProductTypeDTO [id=" + id + ", name=" + name + ", productFamilyId=" + productFamilyId
				+ ", productFamilyName=" + productFamilyName + "]";
	}

}
