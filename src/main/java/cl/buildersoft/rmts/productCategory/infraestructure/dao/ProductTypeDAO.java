package cl.buildersoft.rmts.productCategory.infraestructure.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import cl.buildersoft.rmts.productCategory.domain.entity.ProductTypeEntity;

public interface ProductTypeDAO extends JpaRepository<ProductTypeEntity, Integer> {
	public List<ProductTypeEntity> findByProductFamilyId(Integer productFamilyId);
	
}
