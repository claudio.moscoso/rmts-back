package cl.buildersoft.rmts.productCategory.infraestructure.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cl.buildersoft.rmts.productCategory.application.ProductFamilyService;
import cl.buildersoft.rmts.productCategory.domain.dto.ProductFamilyDTO;

@RestController
@RequestMapping("/api/productFamily/1.0")
public class ProductFamilyController {
	@Autowired
	ProductFamilyService pfs;

	@Secured({ "ROLE_OPER", "ROLE_TECN", "ROLE_SUPE", "ROLE_DEVE" })
	@GetMapping
	public List<ProductFamilyDTO> listAll() {
		List<ProductFamilyDTO> out = pfs.listAll();
		return out;
	}
}
