package cl.buildersoft.rmts.productCategory.infraestructure.dao;


import org.springframework.data.jpa.repository.JpaRepository;

import cl.buildersoft.rmts.productCategory.domain.entity.ProductFamilyEntity;

public interface ProductFamilyDAO extends JpaRepository<ProductFamilyEntity, Integer> {
}
