package cl.buildersoft.rmts.productCategory.infraestructure.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cl.buildersoft.rmts.productCategory.application.ProductTypeService;
import cl.buildersoft.rmts.productCategory.domain.dto.ProductTypeDTO;

@RestController
@RequestMapping("/api/productType/1.0")
public class ProductTypeController {
	@Autowired
	ProductTypeService pfs;

	@Secured({ "ROLE_OPER", "ROLE_TECN", "ROLE_SUPE", "ROLE_DEVE" })
	@GetMapping(value = "byFamily/{productFamilyId}")
	public List<ProductTypeDTO> listByFamily(@PathVariable("productFamilyId") Integer productFamilyId) {
		List<ProductTypeDTO> out = pfs.listByFamilyId(productFamilyId);
		return out;
	}
}
