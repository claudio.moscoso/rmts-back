package cl.buildersoft.rmts.productCategory.application;

import java.util.List;

import cl.buildersoft.rmts.productCategory.domain.dto.ProductFamilyDTO;

public interface ProductFamilyService {

	public List<ProductFamilyDTO> listAll();
}
