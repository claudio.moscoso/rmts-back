package cl.buildersoft.rmts.productCategory.application.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import cl.buildersoft.rmts.productCategory.application.ProductFamilyService;
import cl.buildersoft.rmts.productCategory.domain.dto.ProductFamilyDTO;
import cl.buildersoft.rmts.productCategory.domain.entity.ProductFamilyEntity;
import cl.buildersoft.rmts.productCategory.infraestructure.dao.ProductFamilyDAO;

@Component("ProductFamilyService")
public class ProductFamilyServieImpl implements ProductFamilyService {
	@Autowired
	private ProductFamilyDAO pfd;

	@Autowired
	private ObjectMapper om;

	@Override
	public List<ProductFamilyDTO> listAll() {
		List<ProductFamilyDTO> out = new ArrayList<ProductFamilyDTO>();

		List<ProductFamilyEntity> list = pfd.findAll();
		for (ProductFamilyEntity pfe : list) {
			out.add(om.convertValue(pfe, ProductFamilyDTO.class));
		}

		return out;
	}

}
