package cl.buildersoft.rmts.productCategory.application.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import cl.buildersoft.rmts.productCategory.application.ProductTypeService;
import cl.buildersoft.rmts.productCategory.domain.dto.ProductTypeDTO;
import cl.buildersoft.rmts.productCategory.domain.entity.ProductTypeEntity;
import cl.buildersoft.rmts.productCategory.infraestructure.dao.ProductTypeDAO;

@Component("ProductTypeService")
public class ProductTypeServiceImpl implements ProductTypeService {
	Logger LOG = LoggerFactory.getLogger(ProductTypeServiceImpl.class);
	@Autowired
	ProductTypeDAO productTypeDAO;

	@Autowired
	ObjectMapper om;

	@Override
	public List<ProductTypeDTO> listAll() {
		List<ProductTypeEntity> productTypeList = productTypeDAO.findAll();
		List<ProductTypeDTO> out = new ArrayList<ProductTypeDTO>(productTypeList.size());

		for (ProductTypeEntity pte : productTypeList) {
			out.add(om.convertValue(pte, ProductTypeDTO.class));
		}
		LOG.debug(out.toString());
		return out;
	}

	@Override
	public List<ProductTypeDTO> listByFamilyId(Integer productFamilyId) {
		LOG.debug("Listando todos los tipos de tproductos de la familia {}", productFamilyId);
		List<ProductTypeEntity> entityList = productTypeDAO.findByProductFamilyId(productFamilyId);

		List<ProductTypeDTO> out = new ArrayList<ProductTypeDTO>();
		ProductTypeDTO productType = null;
		for (ProductTypeEntity pte : entityList) {
			productType = new ProductTypeDTO();

			productType.setId(pte.getId());
			productType.setName(pte.getName());
			productType.setProductFamilyId(pte.getProductFamily().getId());
			productType.setProductFamilyName(pte.getProductFamily().getName());

//			try {
//				productType = om.convertValue(pte, ProductTypeDTO.class);
//			} catch (Exception e) {
//				LOG.error(e.getMessage(), e);
//			}
//			productType.setProductFamilyName(pte.getProductFamily().getName());
			out.add(productType);
		}

		LOG.debug(out.toString());

		return out;
	}

}
