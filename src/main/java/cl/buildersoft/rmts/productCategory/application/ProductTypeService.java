package cl.buildersoft.rmts.productCategory.application;

import java.util.List;

import cl.buildersoft.rmts.productCategory.domain.dto.ProductTypeDTO;

public interface ProductTypeService {
	public List<ProductTypeDTO> listAll();

	public List<ProductTypeDTO> listByFamilyId(Integer productFamilyId);
}
