package cl.buildersoft.rmts.product.application;

import java.io.InputStream;
import java.nio.file.Path;
import java.util.List;

import cl.buildersoft.rmts.product.domain.dto.ProductDTO;
import cl.buildersoft.rmts.product.domain.dto.StatusDTO;
import cl.buildersoft.rmts.product.domain.entity.NextStatusEntity;
import cl.buildersoft.rmts.product.domain.entity.ProductEntity;

public interface ProductService {
	public List<ProductDTO> listAll(String name);

	@Deprecated
	public ProductDTO addNew(String smallDesc, Integer productTypeId);

	@Deprecated
	public ProductDTO addNew(String smallDesc, Integer productTypeId, InputStream fileContent, String fileName);

	@Deprecated
	public ProductDTO addNew(String smallDesc, Integer productTypeId, InputStream fileContent, String fileName,
			String comment);

	public ProductDTO addNew(String user, String smallDesc, Integer productTypeId, InputStream fileContent,
			String fileName, String comment);

	public ProductDTO getOne(Integer productId);

	public ProductDTO convertFromEntity(ProductEntity entity);

	public ProductDTO convertFromEntity(ProductEntity entity, List<NextStatusEntity> nextStatus);

	public Path getFile(Integer productId);

	public ProductDTO jumpNextStatus(Integer productId, Integer nextStatus);

	public StatusDTO existsSmallDesc(String smallDesc);

	public StatusDTO retrieveNewId();

	public ProductDTO saveStatusImage(Integer productId, String fileContent);

	public Path readStatusImage(Integer productId);

	public String appendComment(Integer productId, String user, String newComment);
}
