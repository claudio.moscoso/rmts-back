package cl.buildersoft.rmts.product.application.impl;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.DatatypeConverter;

import org.h2.util.DateTimeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import cl.buildersoft.rmts.product.application.ProductService;
import cl.buildersoft.rmts.product.domain.dto.ProductDTO;
import cl.buildersoft.rmts.product.domain.dto.ProductStatusDTO;
import cl.buildersoft.rmts.product.domain.dto.StatusDTO;
import cl.buildersoft.rmts.product.domain.entity.NextStatusEntity;
import cl.buildersoft.rmts.product.domain.entity.ProductEntity;
import cl.buildersoft.rmts.product.domain.entity.ProductStatusEntity;
import cl.buildersoft.rmts.product.infraestructure.dao.NextStatusDAO;
import cl.buildersoft.rmts.product.infraestructure.dao.ProductDAO;
import cl.buildersoft.rmts.product.infraestructure.dao.ProductStatusDAO;
import cl.buildersoft.rmts.productCategory.domain.entity.ProductTypeEntity;
import cl.buildersoft.rmts.productCategory.infraestructure.dao.ProductTypeDAO;
import cl.buildersoft.rmts.utils.application.DateTimeService;

@Component("ProductService")
public class ProductServiceImpl implements ProductService {
	private static final String STATUS_IMAGE = "status-image";
	private static final String TEMPLATE_FOLDER = "template-folder";
	private static final String ISO_8601_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";

	Logger LOG = LoggerFactory.getLogger(ProductServiceImpl.class);
	@Autowired
	ProductDAO productDao;

	@Autowired
	ProductTypeDAO productTypeDao;

	@Autowired
	ProductStatusDAO productStatusDao;

	@Autowired
	NextStatusDAO nextStatusDao;

	@Autowired
	ObjectMapper om;

	@Autowired
	DateTimeService dateTimeService;

	@Value("${cl.buildersoft.rmts.photoFolder}")
	String photoFolder;

	@Override
	public List<ProductDTO> listAll(String name) {
		Map<Integer, List<NextStatusEntity>> statusMap = new HashMap<Integer, List<NextStatusEntity>>();
		List<ProductDTO> out = new ArrayList<ProductDTO>();
		LOG.debug("User: {}", name);
		List<ProductEntity> products = productDao.listByUsername(name);
		List<NextStatusEntity> status = null;

		for (ProductEntity product : products) {
			status = getNextStatus(product, statusMap);

			out.add(convertFromEntity(product, status));
		}

		LOG.debug(out.toString());

		return out;
	}

	private List<NextStatusEntity> getNextStatus(ProductEntity product) {
		return getNextStatus(product, null);
	}

	private List<NextStatusEntity> getNextStatus(ProductEntity product,
			Map<Integer, List<NextStatusEntity>> statusMap) {
		ProductStatusEntity currentStatus = product.getProductStatus();
		if (statusMap == null) {
			return nextStatusDao.findByCurrentStatus(currentStatus);
		}

		List<NextStatusEntity> status = statusMap.get(currentStatus.getId());
		if (status == null) {
			status = nextStatusDao.findByCurrentStatus(currentStatus);
			statusMap.put(currentStatus.getId(), status);
		}

		return status;
	}

	@Override
	public ProductDTO addNew(String smallDesc, Integer productTypeId) {
		return addNew(smallDesc, productTypeId, null, null);
	}

	@Override
	public ProductDTO addNew(String smallDesc, Integer productTypeId, InputStream fileContent, String fileName) {
		return addNew(smallDesc, productTypeId, fileContent, fileName, "");
	}

	@Override
	public ProductDTO addNew(String smallDesc, Integer productTypeId, InputStream fileContent, String fileName,
			String comment) {
		return addNew(null, smallDesc, productTypeId, fileContent, fileName, comment);
	}

	@Override
	public ProductDTO addNew(String user, String smallDesc, Integer productTypeId, InputStream fileContent,
			String fileName, String comment) {
		ProductEntity product = new ProductEntity();
		ProductDTO out = null;
		boolean doSave = true;

		comment = improveComment(user, comment);

		try {
			ProductStatusEntity status = getInitStatus();
			ProductTypeEntity type = productTypeDao.findById(productTypeId).get();

			product.setCreation(Calendar.getInstance());
			product.setProductType(type);
			product.setProductStatus(status);
			product.setSmallDesc(smallDesc);
			product.setComment(comment);
			product = productDao.saveAndFlush(product);

			out = om.convertValue(product, ProductDTO.class);
			out.setProductStatusId(status.getId());
			out.setProductStatusName(status.getName());

			out.setProductTypeId(type.getId());
			out.setProductTypeName(type.getName());
			out.setComment(comment);
		} catch (Exception e) {
			LOG.warn(e.getMessage());
			doSave = false;
		}

		if (doSave) {
			fileName = getFileName(out, fileName);
			Path absolute = Paths.get(photoFolder).toAbsolutePath();
			Path fullPathFileName = Paths.get(photoFolder).resolve(fileName).toAbsolutePath();

			if (LOG.isDebugEnabled()) {
				LOG.debug("photoFolder: {}", absolute.toString());
				LOG.debug("fileName: {}", fileName);
				LOG.debug("fullPathFileName: {}", fullPathFileName.toString());
				LOG.debug("smallDesc: {}", smallDesc);
				LOG.debug("productTypeId: {}", productTypeId);
				LOG.debug("comment: {}", comment);
			}

			File file = new File(fullPathFileName.toString());
			try {
				if (createFolderIfNotExists(absolute.toString())) {
					if (file.exists()) {
						if (!file.delete()) {
							LOG.warn("Cant delete file '{}'", fullPathFileName);
						}
					}
					Files.copy(fileContent, fullPathFileName);
				}
			} catch (IOException e) {
				LOG.error(e.getMessage(), e);
			}
		}
		return out;
	}

	private String improveComment(String user, String comment) {
		return improveComment(user, "", comment);
	}

	private String improveComment(String user, String comment, String newComment) {
		if (comment == null) {
			comment = "";
		}
		if (newComment == null) {
			newComment = "";
		}

		String dateTime = dateTimeService.localDateTimeToString(LocalDateTime.now());
		newComment = dateTime.concat(" (").concat(user).concat("): ").concat(newComment);
		newComment = newComment.concat("\n");

		comment = newComment.concat(comment);

		if (comment.length() > ProductEntity.COMMENT_SIZE) {
			comment = comment.substring(0, ProductEntity.COMMENT_SIZE);
		}
		return comment;
	}

	private String getFileName(ProductDTO productDTO, String fileName) {
		String ext = getExt(fileName);
		return productDTO.getId().toString().concat(ext);
	}

	private String getFileName(Integer productId, String fileName) {
		String ext = getExt(fileName);
		return productId.toString().concat(ext);
	}

	private String getExt(String fileName) {
		return fileName.substring(fileName.lastIndexOf("."));
	}

	private boolean createFolderIfNotExists(String folder) {
		boolean out = true;

		File f = new File(folder);
		if (!f.exists()) {
			if (!f.mkdirs()) {
				LOG.error(String.format("No se pudo crear la carpeta %s", folder));
				out = false;
			}
		}
		return out;
	}

//	@ Bean
	public ProductStatusEntity getInitStatus() {
		List<ProductStatusEntity> status = productStatusDao.findByInitial(true);
		ProductStatusEntity out = null;
		switch (status.size()) {
		case 0:
			throw new RuntimeException("There are not one initial status");
		case 1:
			out = status.get(0);
			break;
		default:
			throw new RuntimeException("There are more than one initial status");
		}

//		return productStatusDao.findByAccessKey("NEW");
		return out;
	}

	@Override
	public ProductDTO getOne(Integer productId) {
		LOG.debug("Buscando producto {}", productId);
		ProductEntity product = productDao.findById(productId).get();
//		List<NextStatusEntity> nextStatus = nextStatusDao.findByCurrentStatus(product.getProductStatus());

		ProductDTO out = convertFromEntity(product);

		return out;
	}

	@Override
	public ProductDTO convertFromEntity(ProductEntity entity, List<NextStatusEntity> nextStatusEntityList) {
		om.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
		om.setDateFormat(new SimpleDateFormat(ISO_8601_FORMAT));

		ProductDTO out = om.convertValue(entity, ProductDTO.class);
		LOG.debug("Entity: {} -> DTO: {}", entity.toString(), out.toString());
		out.setProductTypeId(entity.getProductType().getId());
		out.setProductTypeName(entity.getProductType().getName());

		out.setProductStatusId(entity.getProductStatus().getId());
		out.setProductStatusName(entity.getProductStatus().getName());

		out.setComment(entity.getComment());

		if (nextStatusEntityList == null) {
			nextStatusEntityList = nextStatusDao.findByCurrentStatus(entity.getProductStatus());
		}

		ProductStatusEntity nextStatus = null;
		ProductStatusDTO nextStatusDto = null;
		List<ProductStatusDTO> nextStatusDtoList = new ArrayList<ProductStatusDTO>();
		for (NextStatusEntity nextStatusEntity : nextStatusEntityList) {
			nextStatus = nextStatusEntity.getNextStatus();
			nextStatusDto = new ProductStatusDTO(nextStatus.getId(), nextStatus.getAccessKey(), nextStatus.getName());
			nextStatusDtoList.add(nextStatusDto);
			LOG.debug("Next Status -> {}", nextStatus.toString());
		}
		out.setNextStatus(nextStatusDtoList);

		return out;
	}

	@Override
	public ProductDTO convertFromEntity(ProductEntity entity) {
		return convertFromEntity(entity, null);
	}

	@Override
	public Path getFile(Integer productId) {
		return getFile(photoFolder, productId);
		/**
		 * <code>
		File folder = Paths.get(photoFolder).toAbsolutePath().toFile();
		String fileName = null;
		
		File[] files = folder.listFiles();
		Path out = null;
		for (File file : files) {
			fileName = file.getName();
			if (fileName.substring(0, fileName.indexOf(".")).equals(productId.toString())) {
				out = file.toPath();
				break;
			}
		
			LOG.debug(file.getName());
		}
		
		if (out == null) {
			LOG.warn("File not found for product {}", productId);
		} else {
			LOG.debug("File find '{}'", out.getFileName());
		}
		return out;
		</code>
		 */
	}

	private Path getFile(String relativePath, Integer productId) {
		File folder = Paths.get(relativePath).toAbsolutePath().toFile();
		String fileName = null;

		File[] files = folder.listFiles();
		Path out = null;
		for (File file : files) {
			fileName = file.getName();
			LOG.debug("fileName: '" + fileName + "'");
			if (fileName.indexOf(".") == -1) {
				continue;
			}
			if (fileName.substring(0, fileName.indexOf(".")).equals(productId.toString())) {
				out = file.toPath();
				break;
			}
		}

		if (out == null) {
			LOG.warn("File not found for product {}", productId);
		} else {
			LOG.debug("File find '{}'", out.getFileName());
		}
		return out;
	}

	@Override
	public ProductDTO jumpNextStatus(Integer productId, Integer nextStatus) {
		ProductEntity product = productDao.findById(productId).get();
		boolean validStatus = false;

		List<NextStatusEntity> statusList = getNextStatus(product);
		for (NextStatusEntity status : statusList) {
			if (status.getNextStatus().getId().equals(nextStatus)) {
				validStatus = true;
				break;
			}
		}

		if (!validStatus) {
			throw new RuntimeException("El estado seleccionado, no es valido para este producto");
		}

		product.setProductStatus(productStatusDao.findById(nextStatus).get());
		productDao.saveAndFlush(product);

		return convertFromEntity(product);
	}

	@Override
	public StatusDTO existsSmallDesc(String smallDesc) {
		List<ProductEntity> products = productDao.findBySmallDesc(smallDesc);
		Integer exists = products.size() > 0 ? 1 : 0;
		return new StatusDTO(exists, String.format("SmallDesc %s", exists == 0 ? "not exists." : "exists."));
	}

	@Override
	public StatusDTO retrieveNewId() {
		String currentMax = productDao.findMaxId("mq-%");
		LOG.debug("currentMax from DB:" + currentMax);

		if (currentMax == null) {
			currentMax = "mq-0";
		}

		String number = currentMax.split("-")[1];
		Integer current = Integer.parseInt(number);
		current++;

		return new StatusDTO(0, "mq-" + current);
	}

	@Override
	public ProductDTO saveStatusImage(Integer productId, String fileContent) {
		ProductDTO productDto = null;

		String[] strings = fileContent.split(",");
		String content = strings[1];
		String extension;

		switch (strings[0]) {// check image's extension
		case "data:image/jpeg;base64":
			extension = "jpeg";
			break;
		case "data:image/png;base64":
			extension = "png";
			break;
		default:// should write cases for more images types
			extension = "jpg";
			break;
		}

		String fileName = getFileName(productId, ".".concat(extension));
		Path absolute = Paths.get(photoFolder).resolve(STATUS_IMAGE).toAbsolutePath();
		Path fullPathFileName = Paths.get(photoFolder).resolve(STATUS_IMAGE).resolve(fileName).toAbsolutePath();

		if (LOG.isDebugEnabled()) {
			LOG.debug("photoFolder: {}", absolute.toString());
			LOG.debug("fileName: {}", fileName);
			LOG.debug("fullPathFileName: {}", fullPathFileName.toString());
			LOG.debug("productId: {}", productId);
		}

		byte[] data = DatatypeConverter.parseBase64Binary(content);

		File file = new File(fullPathFileName.toString());
		try {
			if (createFolderIfNotExists(absolute.toString())) {
				if (file.exists()) {
					if (!file.delete()) {
						LOG.warn("Cant delete file '{}'", fullPathFileName);
					}
				}
				OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(file));
				outputStream.write(data);
				outputStream.close();

//				ProductEntity productEntity = updateStatusImage(productId, fileName);
//				productDTO = this.convertFromEntity(productEntity);
			}
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
		}

		return productDto;
	}

	/**
	 * <code>
		private ProductEntity updateStatusImage(Integer productId, String fileName) {
			ProductEntity productEntity = productDao.findById(productId).get();
			productEntity.setStatusImage(fileName);
			productDao.save(productEntity);
			return productEntity;
		}
	</code>
	 */
	@Override
	public Path readStatusImage(Integer productId) {
//		Path out = null;

//		String theirOwnImage = productEntity.getStatusImage();
//		String fileName = null;

		Path theirOwnImage = getFile(Paths.get(photoFolder).resolve(STATUS_IMAGE).toAbsolutePath().toString(),
				productId);
		if (theirOwnImage == null) {
			ProductEntity productEntity = productDao.findById(productId).get();
			theirOwnImage = getFile(Paths.get(photoFolder).resolve(TEMPLATE_FOLDER).toAbsolutePath().toString(),
					productEntity.getProductType().getId());
		}

		/**
		 * <code>
		if (theirOwnImage == null || theirOwnImage.trim().length() == 0) {
			fileName = productEntity.getProductType().getStatusImage();
		} else {
			fileName = theirOwnImage;
		}
		</code>
		 */

		return theirOwnImage;

		/**
		 * <code>
		String out = null;
		
		return out;
		</code>
		 */
	}

	@Override
	public String appendComment(Integer productId, String user, String newComment) {
		ProductEntity product = productDao.findById(productId).get();
		String comment = improveComment(user, product.getComment(), newComment);
		product.setComment(comment);
		
		productDao.saveAndFlush(product);

		return comment;
	}
}
