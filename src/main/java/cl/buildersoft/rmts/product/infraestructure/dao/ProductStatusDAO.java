package cl.buildersoft.rmts.product.infraestructure.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import cl.buildersoft.rmts.product.domain.entity.ProductStatusEntity;

public interface ProductStatusDAO extends JpaRepository<ProductStatusEntity, Integer> {
	public ProductStatusEntity findByAccessKey(String accessKey);

	public List<ProductStatusEntity> findByInitial(Boolean initial);

//	public List<ProductStatusEntity> findByNextStatus(Integer  productStatusEntity);
}
