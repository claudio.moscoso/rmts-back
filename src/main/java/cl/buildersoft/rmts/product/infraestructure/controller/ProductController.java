package cl.buildersoft.rmts.product.infraestructure.controller;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import cl.buildersoft.rmts.product.application.ProductService;
import cl.buildersoft.rmts.product.domain.dto.ProductAndCommentDTO;
import cl.buildersoft.rmts.product.domain.dto.ProductAndStatusDTO;
import cl.buildersoft.rmts.product.domain.dto.ProductDTO;
import cl.buildersoft.rmts.product.domain.dto.StatusDTO;

@RestController
@RequestMapping("/api/product")
public class ProductController {
	Logger LOG = LoggerFactory.getLogger(ProductController.class);
	@Autowired
	ProductService productService;

	@GetMapping(value = "/1.0", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public List<ProductDTO> listAll(Authentication authentication) {
		List<ProductDTO> productList = productService.listAll(authentication.getName());
		return productList;
	}

	@Secured({ "ROLE_OPER", "ROLE_DEVE" })
	@PostMapping(value = "/1.0") // (consumes = MediaType.APPLICATION_JSON_VALUE)
	public ProductDTO addNew1_0(@RequestBody ProductDTO product) throws IOException {
		LOG.debug("{}", product.toString());
		return productService.addNew(product.getSmallDesc(), product.getProductTypeId());
	}

	@Secured({ "ROLE_OPER", "ROLE_DEVE" })
	@PostMapping(value = "/1.1") // , consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ProductDTO> addNew1_1(Authentication authentication,
			@RequestParam("smallDesc") String smallDesc, @RequestParam("productTypeId") Integer productTypeId,
			@RequestParam("photo") MultipartFile photo,
			@RequestParam(name = "comment", required = false) String comment) throws IOException {
		LOG.debug("SmallDesc: {} / ProductTypeId: {} / Photo: {} / Comment: {}", smallDesc, productTypeId,
				photo.getOriginalFilename(), comment);

		ProductDTO response = productService.addNew(authentication.getName(), smallDesc, productTypeId,
				photo.getInputStream(), photo.getOriginalFilename(), comment);

		HttpStatus httpCode = HttpStatus.OK;

		if (response == null) {
			httpCode = HttpStatus.BAD_REQUEST;
		}
		return new ResponseEntity<ProductDTO>(response, httpCode);
	}

	@Secured({ "ROLE_TECN", "ROLE_SUPE", "ROLE_DEVE" })
	@GetMapping(value = "/1.0/{productId}")
	public ProductDTO getProduct(@PathVariable("productId") Integer productId) {
		return productService.getOne(productId);
	}

//	@Secured({ "ROLE_TECN", "ROLE_SUPE", "ROLE_DEVE" })
	// @RolesAllowed({ "ROLE_TECN", "ROLE_SUPE", "ROLE_DEVE" })
	@GetMapping(value = "/1.0/photo/{productId}")
	public ResponseEntity<Resource> getPhoto(@PathVariable("productId") Integer productId) {
		LOG.debug("Descarga de foto para el producto {}", productId);
		ResponseEntity<Resource> out = null;
		Path theFile = productService.getFile(productId);

		Resource file = null;
		try {
			file = new UrlResource(theFile.toUri());
			HttpHeaders headers = new HttpHeaders();
			headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + theFile.getFileName() + "\"");
			out = new ResponseEntity<Resource>(file, headers, HttpStatus.OK);
		} catch (MalformedURLException e) {
			LOG.error(e.getMessage(), e);
			out = new ResponseEntity<Resource>(file, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return out;
	}

	@Secured({ "ROLE_TECN", "ROLE_SUPE", "ROLE_DEVE", "ROLE_OPER" })
	@PostMapping(value = "/1.0/changeStatus")
	public ProductDTO changeStatus(@RequestBody ProductAndStatusDTO productAndStatus) {
		return productService.jumpNextStatus(productAndStatus.getProductId(), productAndStatus.getStatusId());
	}

	@Secured({ "ROLE_DEVE", "ROLE_OPER" })
	@GetMapping(value = "/1.0/existsSmallDesc/{smallDesc}")
	public StatusDTO existsSmallDesc(@PathVariable("smallDesc") String smallDesc) {
		return productService.existsSmallDesc(smallDesc);
	}

	@Secured({ "ROLE_DEVE", "ROLE_OPER" })
	@GetMapping(value = "/1.0/retrieveNewId")
	public StatusDTO retrieveNewId() {
		return productService.retrieveNewId();
	}

	@Secured({ "ROLE_TECN", "ROLE_DEVE" })
	@PostMapping(value = "/1.0/saveStatusImage") // , consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<StatusDTO> saveStatusImage(@RequestParam("productId") Integer productId,
			@RequestParam("image") String base64) throws IOException {
		LOG.debug("ProductId: {} / Photo: {}...", productId, base64.substring(0, 60));
		HttpStatus httpCode = HttpStatus.OK;

		StatusDTO response = new StatusDTO(0, "ok");

		LOG.debug("Image Size: " + base64.length());

		try {
			response.setMessage(productService.saveStatusImage(productId, base64).getId().toString());
		} catch (Exception e) {
			response.setMessage(e.getMessage());
			response.setCode(-1);
			httpCode = HttpStatus.BAD_REQUEST;
		}

		return new ResponseEntity<StatusDTO>(response, httpCode);
	}

//	@Secured({ "ROLE_TECN", "ROLE_DEVE" })
	@GetMapping(value = "/1.0/readStatusImage/{productId}")
	public ResponseEntity<Resource> readStatusImage(@PathVariable("productId") Integer productId) throws IOException {
		LOG.debug("ProductId: {}", productId);

		ResponseEntity<Resource> out = null;
		Path theFile = productService.readStatusImage(productId);

		Resource file = null;
		try {
			file = new UrlResource(theFile.toUri());
			HttpHeaders headers = new HttpHeaders();
			headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + theFile.getFileName() + "\"");
			out = new ResponseEntity<Resource>(file, headers, HttpStatus.OK);
		} catch (MalformedURLException e) {
			LOG.error(e.getMessage(), e);
			out = new ResponseEntity<Resource>(file, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return out;

	}

	@Secured({ "ROLE_TECN", "ROLE_SUPE", "ROLE_DEVE" })
	@PostMapping(value = "/1.0/appendComment")
	public StatusDTO appendComment(Authentication authentication, @RequestBody ProductAndCommentDTO productAndComment) {
		LOG.debug("ProductAndComment: {}", productAndComment.toString());

		StatusDTO out = new StatusDTO(0, productService.appendComment(productAndComment.getProductId(),
				authentication.getName(), productAndComment.getComment()));

		return out;
	}

}
