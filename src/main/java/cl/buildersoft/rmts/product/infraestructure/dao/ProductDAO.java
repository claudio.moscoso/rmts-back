package cl.buildersoft.rmts.product.infraestructure.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import cl.buildersoft.rmts.product.domain.entity.ProductEntity;

public interface ProductDAO extends JpaRepository<ProductEntity, Integer> {
	public List<ProductEntity> findBySmallDesc(String smallDesc);

//	
	@Query(nativeQuery = true, //
			value = "SELECT * \n" + //
					"FROM product \n" + //
					"WHERE product_status_id IN (\n" + //
					"	SELECT product_status_id \n" + //
					"	FROM role_status rs \n" + //
					"	WHERE role_id in (\n" + //
					"		SELECT role_id \n" + //
					"		FROM user_role ur \n" + //
					"		WHERE user_id in (\n" + //
					"			SELECT id \n" + //
					"			FROM `user` u\n" + //
					"			WHERE username = ?1\n" + //
					"		)\n" + //
					"	)\n" + //
					")" //
	)
	public List<ProductEntity> listByUsername(String username);

	@Query("SELECT max(p.smallDesc) FROM ProductEntity p WHERE p.smallDesc like ?1")
	public String findMaxId(String smallDesc);

}
