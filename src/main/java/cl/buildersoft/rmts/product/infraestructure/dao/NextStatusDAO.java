package cl.buildersoft.rmts.product.infraestructure.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import cl.buildersoft.rmts.product.domain.entity.NextStatusEntity;
import cl.buildersoft.rmts.product.domain.entity.ProductStatusEntity;

public interface NextStatusDAO extends JpaRepository<NextStatusEntity, Integer> {
	// ProductStatusEntity currentStatus
	public List<NextStatusEntity> findByCurrentStatus(ProductStatusEntity currentStatus);
}
