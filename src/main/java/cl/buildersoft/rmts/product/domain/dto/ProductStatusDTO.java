package cl.buildersoft.rmts.product.domain.dto;

public class ProductStatusDTO {
	private Integer id;
	private String accessKey;
	private String name;

	public ProductStatusDTO() {
	}

	public ProductStatusDTO(Integer id, String accessKey, String name) {
		super();
		this.id = id;
		this.accessKey = accessKey;
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAccessKey() {
		return accessKey;
	}

	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "ProductStatusDTO [id=" + id + ", accessKey=" + accessKey + ", name=" + name + "]";
	}

}
