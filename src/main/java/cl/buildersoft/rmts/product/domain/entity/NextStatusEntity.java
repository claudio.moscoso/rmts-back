package cl.buildersoft.rmts.product.domain.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "next_status")
public class NextStatusEntity implements Serializable {
	private static final long serialVersionUID = -5117413848092100915L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@ManyToOne
	private ProductStatusEntity currentStatus;

	@ManyToOne
	private ProductStatusEntity nextStatus;

	public NextStatusEntity() {
	}

	public NextStatusEntity(Integer id, ProductStatusEntity currentStatus, ProductStatusEntity nextStatus) {
		super();
		this.id = id;
		this.currentStatus = currentStatus;
		this.nextStatus = nextStatus;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ProductStatusEntity getCurrentStatus() {
		return currentStatus;
	}

	public void setCurrentStatus(ProductStatusEntity currentStatus) {
		this.currentStatus = currentStatus;
	}

	public ProductStatusEntity getNextStatus() {
		return nextStatus;
	}

	public void setNextStatus(ProductStatusEntity nextStatus) {
		this.nextStatus = nextStatus;
	}

	@Override
	public String toString() {
		return "NextStatusEntity [id=" + id + ", currentStatus=" + currentStatus + ", nextStatus=" + nextStatus + "]";
	}

}
