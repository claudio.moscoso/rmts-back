package cl.buildersoft.rmts.product.domain.dto;

import java.util.Date;
import java.util.List;

public class ProductDTO {
	private Integer id;

	private Integer productStatusId;
	private String productStatusName;

	private Integer productTypeId;
	private String productTypeName;

	private String smallDesc;
	private Date creation;

	private List<ProductStatusDTO> nextStatus;
	private String comment;

	public ProductDTO() {
	}

	public ProductDTO(Integer id, Integer productStatusId, String productStatusName, Integer productTypeId,
			String productTypeName, String smallDesc, Date creation, List<ProductStatusDTO> nextStatus,
			String comment) {
		super();
		this.id = id;
		this.productStatusId = productStatusId;
		this.productStatusName = productStatusName;
		this.productTypeId = productTypeId;
		this.productTypeName = productTypeName;
		this.smallDesc = smallDesc;
		this.creation = creation;
		this.nextStatus = nextStatus;
		this.comment = comment;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getProductStatusId() {
		return productStatusId;
	}

	public void setProductStatusId(Integer productStatusId) {
		this.productStatusId = productStatusId;
	}

	public String getProductStatusName() {
		return productStatusName;
	}

	public void setProductStatusName(String productStatusName) {
		this.productStatusName = productStatusName;
	}

	public Integer getProductTypeId() {
		return productTypeId;
	}

	public void setProductTypeId(Integer productTypeId) {
		this.productTypeId = productTypeId;
	}

	public String getProductTypeName() {
		return productTypeName;
	}

	public void setProductTypeName(String productTypeName) {
		this.productTypeName = productTypeName;
	}

	public String getSmallDesc() {
		return smallDesc;
	}

	public void setSmallDesc(String smallDesc) {
		this.smallDesc = smallDesc;
	}

	public Date getCreation() {
		return creation;
	}

	public void setCreation(Date creation) {
		this.creation = creation;
	}

	public List<ProductStatusDTO> getNextStatus() {
		return nextStatus;
	}

	public void setNextStatus(List<ProductStatusDTO> nextStatus) {
		this.nextStatus = nextStatus;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	@Override
	public String toString() {
		return "ProductDTO [id=" + id + ", productStatusId=" + productStatusId + ", productStatusName="
				+ productStatusName + ", productTypeId=" + productTypeId + ", productTypeName=" + productTypeName
				+ ", smallDesc=" + smallDesc + ", creation=" + creation + ", nextStatus=" + nextStatus + ", comment="
				+ comment + "]";
	}

}
