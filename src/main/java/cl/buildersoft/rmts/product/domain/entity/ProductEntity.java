package cl.buildersoft.rmts.product.domain.entity;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import cl.buildersoft.rmts.productCategory.domain.entity.ProductTypeEntity;

@Entity
@Table(name = "product")
public class ProductEntity implements Serializable {
	public static final int COMMENT_SIZE = 65535;

	private static final long serialVersionUID = -1043961308889057996L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@ManyToOne(optional = false)
	private ProductStatusEntity productStatus;

	@ManyToOne(optional = false)
	private ProductTypeEntity productType;

//	@Column
//	private String photo;

	@Column(length = 280)
	private String smallDesc;

	@Column
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar creation;

	@Column(length = COMMENT_SIZE)
	private String comment;

	public ProductEntity() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ProductStatusEntity getProductStatus() {
		return productStatus;
	}

	public void setProductStatus(ProductStatusEntity productStatus) {
		this.productStatus = productStatus;
	}

	public ProductTypeEntity getProductType() {
		return productType;
	}

	public void setProductType(ProductTypeEntity productType) {
		this.productType = productType;
	}

	public String getSmallDesc() {
		return smallDesc;
	}

	public void setSmallDesc(String smallDesc) {
		this.smallDesc = smallDesc;
	}

	public Calendar getCreation() {
		return creation;
	}

	public void setCreation(Calendar creation) {
		this.creation = creation;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	@Override
	public String toString() {
		return "ProductEntity [id=" + id + ", productStatus=" + productStatus + ", productType=" + productType
				+ ", smallDesc=" + smallDesc + ", creation=" + creation + ", comment=" + comment + "]";
	}

}
