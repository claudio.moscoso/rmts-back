package cl.buildersoft.rmts.product.domain.dto;

import java.io.Serializable;

public class ProductAndStatusDTO implements Serializable {
	private static final long serialVersionUID = 2168141454525123868L;
	private Integer productId;
	private Integer statusId;

	public ProductAndStatusDTO() {
	}

	public ProductAndStatusDTO(Integer productId, Integer statusId) {
		super();
		this.productId = productId;
		this.statusId = statusId;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Integer getStatusId() {
		return statusId;
	}

	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}

	@Override
	public String toString() {
		return "ProductAndStatusDTO [productId=" + productId + ", statusId=" + statusId + "]";
	}
}
