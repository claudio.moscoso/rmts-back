package cl.buildersoft.rmts.product.domain.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "product_status")
public class ProductStatusEntity implements Serializable {
	private static final long serialVersionUID = 1290754328626041690L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(length = 16)
	private String accessKey;

	@Column(length = 60)
	private String name;

	@Column(nullable = false)
	private Boolean initial;

	@Column(nullable = false)
	private Boolean latest;

//	@OneToMany
//	@JoinColumn(unique = false, name="id")
//	private List<ProductStatusEntity> nextStatus;

	public ProductStatusEntity() {
	}
	public ProductStatusEntity(Integer id, String accessKey, String name, Boolean initial, Boolean latest) {
		super();
		this.id = id;
		this.accessKey = accessKey;
		this.name = name;
		this.initial = initial;
		this.latest = latest;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getAccessKey() {
		return accessKey;
	}
	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Boolean getInitial() {
		return initial;
	}
	public void setInitial(Boolean initial) {
		this.initial = initial;
	}
	public Boolean getLatest() {
		return latest;
	}
	public void setLatest(Boolean latest) {
		this.latest = latest;
	}
	@Override
	public String toString() {
		return "ProductStatusEntity [id=" + id + ", accessKey=" + accessKey + ", name=" + name + ", initial=" + initial
				+ ", latest=" + latest + "]";
	}

	  

}
