package cl.buildersoft.rmts.product.domain.dto;

public class StatusDTO {
	private Integer code;
	private String message;

	public StatusDTO() {
	}

	public StatusDTO(Integer code, String message) {
		super();
		this.code = code;
		this.message = message;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "StatusDTO [code=" + code + ", message=" + message + "]";
	}

}
