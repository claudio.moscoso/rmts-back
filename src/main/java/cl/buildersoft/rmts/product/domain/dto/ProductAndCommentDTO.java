package cl.buildersoft.rmts.product.domain.dto;

import java.io.Serializable;

public class ProductAndCommentDTO implements Serializable {
	private static final long serialVersionUID = -4541052109484200814L;
	private Integer productId = null;
	private String comment = null;

	public ProductAndCommentDTO() {
	}

	public ProductAndCommentDTO(Integer productId, String comment) {
		super();
		this.productId = productId;
		this.comment = comment;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	@Override
	public String toString() {
		return "ProductAndCommentDTO [productId=" + productId + ", comment=" + comment + "]";
	}

}
