package cl.buildersoft.rmts.product.domain.entity;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "product_history")
public class ProductHistoryEntity implements Serializable {
	private static final long serialVersionUID = 1625428966641629314L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar creation;

	@Column(length = 280)
	private String changeDesc;

	@ManyToOne(fetch = FetchType.LAZY)
	private ProductEntity product;

	public ProductHistoryEntity() {
	}

	public ProductHistoryEntity(Integer id, Calendar creation, String changeDesc, ProductEntity product) {
		super();
		this.id = id;
		this.creation = creation;
		this.changeDesc = changeDesc;
		this.product = product;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Calendar getCreation() {
		return creation;
	}

	public void setCreation(Calendar creation) {
		this.creation = creation;
	}

	public String getChangeDesc() {
		return changeDesc;
	}

	public void setChangeDesc(String changeDesc) {
		this.changeDesc = changeDesc;
	}

	public ProductEntity getProduct() {
		return product;
	}

	public void setProduct(ProductEntity product) {
		this.product = product;
	}

	@Override
	public String toString() {
		return "ProductHistoryEntity [id=" + id + ", creation=" + creation + ", changeDesc=" + changeDesc + ", product="
				+ product + "]";
	}

}
