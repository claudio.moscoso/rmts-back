package cl.buildersoft.rmts;

import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import cl.buildersoft.rmts.field.application.impl.FieldServiceImpl;

@SpringBootApplication
public class RmtsBackApplication implements CommandLineRunner {
	Logger LOG = LoggerFactory.getLogger(RmtsBackApplication.class);

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	public static void main(String[] args) {
		SpringApplication.run(RmtsBackApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		LOG.debug("args: {}", args.length);

		for (int i = 0; i < args.length; i++) {
			LOG.debug("arg: {}", args[i]);
		}

		String password = args.length > 0 ? args[0] : null;
		//password="123456";
		if (password != null) {
			for (int i = 0; i < 4; i++) {
				LOG.debug(passwordEncoder.encode(password));
			}
		}
	}

}
