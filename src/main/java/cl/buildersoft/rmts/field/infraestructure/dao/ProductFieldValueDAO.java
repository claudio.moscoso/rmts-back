package cl.buildersoft.rmts.field.infraestructure.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import cl.buildersoft.rmts.field.domain.entity.FieldEntity;
import cl.buildersoft.rmts.field.domain.entity.FieldValueEntity;
import cl.buildersoft.rmts.product.domain.entity.ProductEntity;

public interface ProductFieldValueDAO extends JpaRepository<FieldValueEntity, Integer> {
//	public RoleTypeStatusFieldEntity findByProductAndField(ProductEntity product, FieldEntity field);
	public FieldValueEntity findByProductAndField(ProductEntity product, FieldEntity field);

//	public List<ProductFieldValueEntity> listByProductAndField(Integer product, Integer field);
}
