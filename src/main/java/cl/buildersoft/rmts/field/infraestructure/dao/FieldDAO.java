package cl.buildersoft.rmts.field.infraestructure.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import cl.buildersoft.rmts.field.domain.entity.FieldEntity;

public interface FieldDAO extends JpaRepository<FieldEntity, Integer>, FieldCustomDAO {
	public List<FieldEntity> findByProductTypeId(Integer productTypeId);

}
