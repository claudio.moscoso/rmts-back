package cl.buildersoft.rmts.field.infraestructure.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import cl.buildersoft.rmts.field.domain.entity.RoleTypeStatusFieldEntity;
import cl.buildersoft.rmts.product.domain.entity.ProductStatusEntity;
import cl.buildersoft.rmts.productCategory.domain.entity.ProductTypeEntity;
import cl.buildersoft.rmts.user.domain.entity.RoleEntity;

public interface RoleTypeStatusFieldDAO extends JpaRepository<RoleTypeStatusFieldEntity, Integer> {
	public List<RoleTypeStatusFieldEntity> findByRoleAndProductTypeAndProductStatus(Integer roleId,
			Integer productTypeId, Integer productStatusId);

	public List<RoleTypeStatusFieldEntity> findByRoleAndProductTypeAndProductStatus(RoleEntity roleId,
			ProductTypeEntity productTypeId, ProductStatusEntity productStatusId);

}
