package cl.buildersoft.rmts.field.infraestructure.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import cl.buildersoft.rmts.field.domain.entity.FieldEntity;

public interface FieldCustomDAO {
	@Query("SELECT f FROM FieldEntity f WHERE f.id IN (:ids)")
	public List<FieldEntity> listByList(Integer[] ids);

}
