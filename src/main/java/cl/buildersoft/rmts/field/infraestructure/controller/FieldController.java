package cl.buildersoft.rmts.field.infraestructure.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cl.buildersoft.rmts.field.application.FieldService;
import cl.buildersoft.rmts.field.domain.dto.FieldDTO;
import cl.buildersoft.rmts.field.domain.dto.FieldValueDTO;
import cl.buildersoft.rmts.product.domain.dto.StatusDTO;

@RestController
@RequestMapping("/api/field/1.0")
public class FieldController {
	Logger LOG = LoggerFactory.getLogger(FieldController.class);

	@Autowired
	FieldService fieldService;

	@Secured({ "ROLE_TECN", "ROLE_SUPE", "ROLE_DEVE" })
	@GetMapping(value = "/listByPrdoduct/{productId}")
	public List<FieldDTO> listByProductId(@PathVariable("productId") Integer productId, Authentication authentication) {
//		LOG.debug("Authentication:'{}'",authentication.getName());
		String userName = authentication.getName();
		return fieldService.listByProductAndUser(productId, userName);
	}

	/**
	 * <code>
	&#64;PostMapping(value = "/saveList", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public StatusDTO saveList(@RequestBody List<FieldValueDTO> listFieldValue) {
		StatusDTO out = new StatusDTO(0, "");
		LOG.debug("Lista de entrada: {}", listFieldValue.toString());
		return out;
	}
	</code>
	 */
	@Secured({ "ROLE_TECN", "ROLE_SUPE", "ROLE_DEVE" })
	@PostMapping(value = "/saveOne", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public StatusDTO saveOne(@RequestBody FieldValueDTO fieldValue) {
		StatusDTO out = new StatusDTO(0, "");
		LOG.debug("Campo para grabar: {}", fieldValue.toString());

		fieldService.saveValue(fieldValue.getProduct(), fieldValue.getField(), fieldValue.getValue());

		return out;
	}

}
