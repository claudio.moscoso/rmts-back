package cl.buildersoft.rmts.field.domain.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import cl.buildersoft.rmts.product.domain.entity.ProductEntity;

@Entity
@Table(name = "field_value")
public class FieldValueEntity implements Serializable {
	private static final long serialVersionUID = 1853559727597854055L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@ManyToOne
	private FieldEntity field;

	@ManyToOne
	private ProductEntity product;

	@Column
	private String value;

	public FieldValueEntity() {
	}

	public FieldValueEntity(Integer id, FieldEntity field, ProductEntity product, String value) {
		super();
		this.id = id;
		this.field = field;
		this.product = product;
		this.value = value;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public FieldEntity getField() {
		return field;
	}

	public void setField(FieldEntity field) {
		this.field = field;
	}

	public ProductEntity getProduct() {
		return product;
	}

	public void setProduct(ProductEntity product) {
		this.product = product;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "FieldValueEntity [id=" + id + ", field=" + field + ", product=" + product + ", value=" + value + "]";
	}

}
