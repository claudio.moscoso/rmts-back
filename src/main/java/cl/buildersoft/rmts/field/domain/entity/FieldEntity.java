package cl.buildersoft.rmts.field.domain.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import cl.buildersoft.rmts.field.domain.enumeration.FieldTypeEnum;
import cl.buildersoft.rmts.productCategory.domain.entity.ProductTypeEntity;

@Entity
@Table(name = "field")
public class FieldEntity implements Serializable {
	private static final long serialVersionUID = -8008982774093015171L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(length = 50)
	private String name;

	@Enumerated(EnumType.STRING)
	private FieldTypeEnum type;

	@ManyToOne
	private ProductTypeEntity productType;

	public FieldEntity() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public FieldTypeEnum getType() {
		return type;
	}

	public void setType(FieldTypeEnum type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "FieldEntity [id=" + id + ", name=" + name + ", type=" + type + ", productType=" + productType + "]";
	}

}
