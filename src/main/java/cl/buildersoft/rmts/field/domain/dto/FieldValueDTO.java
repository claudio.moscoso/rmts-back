package cl.buildersoft.rmts.field.domain.dto;

public class FieldValueDTO {
	private Integer product;
	private Integer field;
	private String value;

	public FieldValueDTO() {
	}

	public FieldValueDTO(Integer product, Integer field, String value) {
		super();
		this.product = product;
		this.field = field;
		this.value = value;
	}

	public Integer getProduct() {
		return product;
	}

	public void setProduct(Integer product) {
		this.product = product;
	}

	public Integer getField() {
		return field;
	}

	public void setField(Integer field) {
		this.field = field;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "FieldValueDTO [product=" + product + ", field=" + field + ", value=" + value + "]";
	}

}
