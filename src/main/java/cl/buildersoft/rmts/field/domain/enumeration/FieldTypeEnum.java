package cl.buildersoft.rmts.field.domain.enumeration;

public enum FieldTypeEnum {
	DATE, DATETIME, EMAIL, NUMBER, TEXT, TIME
}
