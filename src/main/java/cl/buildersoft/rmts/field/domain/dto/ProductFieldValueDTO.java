package cl.buildersoft.rmts.field.domain.dto;

public class ProductFieldValueDTO {
	private Integer productId;
	private Integer fieldId;
	private String value;

	public ProductFieldValueDTO() {
	}

	public ProductFieldValueDTO(Integer productId, Integer fieldId, String value) {
		super();
		this.productId = productId;
		this.fieldId = fieldId;
		this.value = value;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Integer getFieldId() {
		return fieldId;
	}

	public void setFieldId(Integer fieldId) {
		this.fieldId = fieldId;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "ProductFieldValueDTO [productId=" + productId + ", fieldId=" + fieldId + ", value=" + value + "]";
	}

}
