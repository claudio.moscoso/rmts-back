package cl.buildersoft.rmts.field.domain.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import cl.buildersoft.rmts.product.domain.entity.ProductStatusEntity;
import cl.buildersoft.rmts.productCategory.domain.entity.ProductTypeEntity;
import cl.buildersoft.rmts.user.domain.entity.RoleEntity;

@Entity
@Table(name = "role_type_status_field")
public class RoleTypeStatusFieldEntity implements Serializable {
	private static final long serialVersionUID = -8573505067043737254L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@ManyToOne
	private RoleEntity role;

	@ManyToOne
	private ProductTypeEntity productType;

	@ManyToOne
	private ProductStatusEntity productStatus;

	@ManyToOne
	private FieldEntity field;

	@Column(columnDefinition = "boolean default false")
	private Boolean readonly;

	public RoleTypeStatusFieldEntity() {
	}

	public RoleTypeStatusFieldEntity(Integer id, RoleEntity role, ProductTypeEntity productType,
			ProductStatusEntity productStatus, FieldEntity field, Boolean readonly) {
		super();
		this.id = id;
		this.role = role;
		this.productType = productType;
		this.productStatus = productStatus;
		this.field = field;
		this.readonly = readonly;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public RoleEntity getRole() {
		return role;
	}

	public void setRole(RoleEntity role) {
		this.role = role;
	}

	public ProductTypeEntity getProductType() {
		return productType;
	}

	public void setProductType(ProductTypeEntity productType) {
		this.productType = productType;
	}

	public ProductStatusEntity getProductStatus() {
		return productStatus;
	}

	public void setProductStatus(ProductStatusEntity productStatus) {
		this.productStatus = productStatus;
	}

	public FieldEntity getField() {
		return field;
	}

	public void setField(FieldEntity field) {
		this.field = field;
	}

	public Boolean getReadonly() {
		return readonly;
	}

	public void setReadonly(Boolean readonly) {
		this.readonly = readonly;
	}

	@Override
	public String toString() {
		return "RoleTypeStatusFieldEntity [id=" + id + ", role=" + role + ", productType=" + productType
				+ ", productStatus=" + productStatus + ", field=" + field + ", readonly=" + readonly + "]";
	}

}
