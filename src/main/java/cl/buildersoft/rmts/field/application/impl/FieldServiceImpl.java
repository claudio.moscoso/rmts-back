package cl.buildersoft.rmts.field.application.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import cl.buildersoft.rmts.field.application.FieldService;
import cl.buildersoft.rmts.field.domain.dto.FieldDTO;
import cl.buildersoft.rmts.field.domain.dto.FieldValueDTO;
import cl.buildersoft.rmts.field.domain.entity.FieldEntity;
import cl.buildersoft.rmts.field.domain.entity.FieldValueEntity;
import cl.buildersoft.rmts.field.domain.entity.RoleTypeStatusFieldEntity;
import cl.buildersoft.rmts.field.infraestructure.dao.FieldDAO;
import cl.buildersoft.rmts.field.infraestructure.dao.ProductFieldValueDAO;
import cl.buildersoft.rmts.field.infraestructure.dao.RoleTypeStatusFieldDAO;
import cl.buildersoft.rmts.product.domain.entity.ProductEntity;
import cl.buildersoft.rmts.product.domain.entity.ProductStatusEntity;
import cl.buildersoft.rmts.product.infraestructure.dao.ProductDAO;
import cl.buildersoft.rmts.productCategory.domain.entity.ProductTypeEntity;
import cl.buildersoft.rmts.user.domain.entity.RoleEntity;
import cl.buildersoft.rmts.user.domain.entity.UserEntity;
import cl.buildersoft.rmts.user.infraestructure.dao.RoleDAO;
import cl.buildersoft.rmts.user.infraestructure.dao.UserDAO;

@Service("FieldService")
public class FieldServiceImpl implements FieldService {
	Logger LOG = LoggerFactory.getLogger(FieldServiceImpl.class);
	@Autowired
	private FieldDAO fieldDao;

	@Autowired
	private ProductDAO productDao;

	@Autowired
	private ProductFieldValueDAO productFieldValueDao;

	@Autowired
	ObjectMapper om;

	@Autowired
	private UserDAO userDao;

	private RoleDAO roleDao;

	@Autowired
	private RoleTypeStatusFieldDAO roleTypeStatusFieldDao;

	@Override
	public List<FieldDTO> listByProductId(Integer productId) {
		ProductEntity product = productDao.findById(productId).orElse(null);
		Integer productTypeId = product.getProductType().getId();
		String value;
		FieldDTO fieldValue;
		List<FieldDTO> out = new ArrayList<FieldDTO>();

		List<FieldEntity> typeFields = fieldDao.findByProductTypeId(productTypeId);

		for (FieldEntity field : typeFields) {
			if (LOG.isDebugEnabled()) {
				LOG.debug(field.toString());
			}
			fieldValue = om.convertValue(field, FieldDTO.class);
			value = findValue(product, field);
			fieldValue.setValue(value);

			out.add(fieldValue);

		}

		return out;
	}

	private String findValue(ProductEntity product, FieldEntity field) {
		FieldValueEntity valueStored = productFieldValueDao.findByProductAndField(product, field);
		return valueStored == null ? "" : valueStored.getValue();
	}

	@Override
	public FieldValueDTO saveValue(Integer productId, Integer fieldId, String value) {
		FieldValueDTO out = null;
		ProductEntity product = productDao.findById(productId).orElseGet(null);
		FieldEntity field = fieldDao.findById(fieldId).orElseGet(null);
		FieldValueEntity fieldValueEntity = null;

		fieldValueEntity = productFieldValueDao.findByProductAndField(product, field);
		LOG.debug("Entity found='{}'", fieldValueEntity == null ? "null" : fieldValueEntity.toString());
//		ProductFieldValueEntity productFieldValues = productFieldValueDao.findByProductAndField(productId, fieldId);

//		throw new RuntimeException("Falta completar desarrollo");

		if (fieldValueEntity == null) {
			fieldValueEntity = new FieldValueEntity(null, field, product, value);
		} else {
			fieldValueEntity.setValue(value);
		}

		fieldValueEntity = productFieldValueDao.saveAndFlush(fieldValueEntity);
		LOG.debug("Entity saved '{}'", fieldValueEntity.toString());
		out = new FieldValueDTO(fieldValueEntity.getProduct().getId(), fieldValueEntity.getField().getId(), value);

		return out;
	}

	@Override
	public List<FieldDTO> listByProductAndUser(Integer productId, String user) {
// RoleTypeStatus->Field
		List<FieldDTO> out = new ArrayList<FieldDTO>();
		List<RoleEntity> rols = getRolsByUser(user);
		List<RoleEntity> complementRols = getComplements(rols);
		FieldDTO fieldValueDto = null;
		ProductEntity product = productDao.findById(productId).get();
		String value;

		ProductTypeEntity productTypeEntity = product.getProductType();
		ProductStatusEntity productStatusEntity = product.getProductStatus();

		Set<RoleTypeStatusFieldEntity> fieldsSet = new HashSet<RoleTypeStatusFieldEntity>();
		List<RoleTypeStatusFieldEntity> fieldsTemp;
		fillRols(rols, productTypeEntity, productStatusEntity, fieldsSet);

		int i = 0;
		Integer[] ids = new Integer[fieldsSet.size()];
		for (RoleTypeStatusFieldEntity field : fieldsSet) {
			ids[i++] = field.getId();
		}

		if (ids.length > 0) {
			List<FieldEntity> out2 = fieldDao.listByList(ids);
			for (FieldEntity field : out2) {
				fieldValueDto = om.convertValue(field, FieldDTO.class);
				value = findValue(product, field);
				fieldValueDto.setValue(value);

				out.add(fieldValueDto);

			}
		}
		return out;
	}

	private void fillRols(List<RoleEntity> rols, ProductTypeEntity productTypeEntity,
			ProductStatusEntity productStatusEntity, Set<RoleTypeStatusFieldEntity> fieldsSet) {
		List<RoleTypeStatusFieldEntity> fieldsTemp;
		for (RoleEntity role : rols) {
			LOG.debug("Buscar por rol:{} ProductType:{} Status:{}", role.getId(), productTypeEntity.getId(),
					productStatusEntity.getId());
			fieldsTemp = roleTypeStatusFieldDao.findByRoleAndProductTypeAndProductStatus(role, productTypeEntity,
					productStatusEntity);
//			fieldsTemp = roleTypeStatusFieldDao.findByRoleAndProductTypeAndProductStatus(role.getId(), productTypeId,
//					productStatusId);

			fieldsSet.addAll(fieldsTemp);
		}
	}

	private List<RoleEntity> getComplements(List<RoleEntity> rols) {
		List<RoleEntity> out = new ArrayList<RoleEntity>();
		List<RoleEntity> allRols = roleDao.findAll();
		boolean exists = false;

		for (RoleEntity i : allRols) {
			exists = false;
			for (RoleEntity j : rols) {
				if (i.getId().equals(j.getId())) {
					exists = true;
					break;
				}
			}
			if (!exists) {
				out.add(i);
			}
		}

		return out;
	}

	private List<RoleEntity> getRolsByUser(String userName) {
		UserEntity userEntity = userDao.findByUsername(userName);
		List<RoleEntity> rols = userEntity.getRoles();
		return rols;
	}

}
