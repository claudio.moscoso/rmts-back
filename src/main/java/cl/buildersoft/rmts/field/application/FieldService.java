package cl.buildersoft.rmts.field.application;

import java.util.List;

import cl.buildersoft.rmts.field.domain.dto.FieldDTO;
import cl.buildersoft.rmts.field.domain.dto.FieldValueDTO;

public interface FieldService {
	@Deprecated
	public List<FieldDTO> listByProductId(Integer productId);

	public List<FieldDTO> listByProductAndUser(Integer productId, String user);

	public FieldValueDTO saveValue(Integer productId, Integer fieldId, String value);
}
