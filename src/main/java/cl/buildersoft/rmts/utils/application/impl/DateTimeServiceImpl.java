package cl.buildersoft.rmts.utils.application.impl;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import org.springframework.stereotype.Service;

import cl.buildersoft.rmts.utils.application.DateTimeService;

@Service("DateTimeService")
public class DateTimeServiceImpl implements DateTimeService {
	@Override
	public LocalDate stringToLocalDate(String dateAsString, String pattern) {
		return stringToLocalDate(dateAsString, pattern, null);
	}
	@Override
	public LocalDate stringToLocalDate(String dateAsString, String pattern, Locale locale) {
		DateTimeFormatter formatter = locale == null ? DateTimeFormatter.ofPattern(pattern)
				: DateTimeFormatter.ofPattern(pattern, locale);
		LocalDate out = LocalDate.parse(dateAsString, formatter);
		return out;
	}

	@Override
	public String localDateToString(LocalDate localDate, String pattern) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
		String out = formatter.format(localDate);
		return out;
	}

	@Override
	public String convertFormat(String value, String fromPattern, String toPattern) {
		LocalDate dateTime = stringToLocalDate(value, fromPattern);
		String out = localDateToString(dateTime, toPattern);
		return out;
	}

	@Override
	public String convertFormat(String value, String fromPattern, Locale fromLocale, String toPattern) {
		LocalDate dateTime = stringToLocalDate(value, fromPattern, fromLocale);
		String out = localDateToString(dateTime, toPattern);
		return out;
	}

	@Override
	public String localDateTimeToString(LocalDateTime localDateTime, String pattern) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
		String out = formatter.format(localDateTime);
		return out;
	}

	@Override
	public String localDateTimeToString(LocalDateTime localDate) {
		return localDateTimeToString(localDate, "dd/MM/yyyy HH:mm:ss");
	}

}
