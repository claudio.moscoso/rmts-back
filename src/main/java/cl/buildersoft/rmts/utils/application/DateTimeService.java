package cl.buildersoft.rmts.utils.application;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Locale;

public interface DateTimeService {
	LocalDate stringToLocalDate(String dateAsString, String pattern);

	LocalDate stringToLocalDate(String dateAsString, String pattern, Locale locale);

	String localDateToString(LocalDate localDate, String pattern);

	String convertFormat(String value, String fromPattern, String toPattern);

	String convertFormat(String value, String fromPattern, Locale fromLocale, String toPattern);

	String localDateTimeToString(LocalDateTime localDateTime, String pattern);

	String localDateTimeToString(LocalDateTime localDate);


}
