package cl.buildersoft.rmts.user.application.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cl.buildersoft.rmts.user.application.UserService;
import cl.buildersoft.rmts.user.domain.entity.UserEntity;
import cl.buildersoft.rmts.user.infraestructure.dao.UserDAO;

@Service
public class UserDetailServiceImpl implements UserDetailsService, UserService {
	Logger LOG = LoggerFactory.getLogger(UserDetailServiceImpl.class);
	@Autowired
	private UserDAO userDao;

	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserEntity userEntity = userDao.findByUsername(username);

		if (userEntity == null) {
			String msg = String.format("El usuario '%s' no existe", username);
			LOG.warn(msg);
			throw new UsernameNotFoundException(msg);
		}

		List<GrantedAuthority> authorities = userEntity.getRoles()//
				.stream()//
				.map(role -> new SimpleGrantedAuthority(role.getName()))//
				.peek(autority -> LOG.debug(autority.getAuthority()))//
				.collect(Collectors.toList());

		UserDetails out = new User(username, userEntity.getPassword(), userEntity.getEnabled(), true, true, true,
				authorities);

		return out;
	}

	@Override
	public UserEntity findByUsername(String username) {
		return userDao.findByUsername(username);
	}

}
