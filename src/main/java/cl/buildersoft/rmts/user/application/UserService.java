package cl.buildersoft.rmts.user.application;

import cl.buildersoft.rmts.user.domain.entity.UserEntity;

public interface UserService {
	public UserEntity findByUsername(String username);
}
