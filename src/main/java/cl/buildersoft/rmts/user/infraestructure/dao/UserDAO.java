package cl.buildersoft.rmts.user.infraestructure.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import cl.buildersoft.rmts.user.domain.entity.UserEntity;

public interface UserDAO extends JpaRepository<UserEntity, Integer> {
	public UserEntity findByUsername(String username);

	@Query("select u from UserEntity u where u.username=?1 and u.password=?2")
	public UserEntity findByUsernameAndPassword(String username, String password);

}
