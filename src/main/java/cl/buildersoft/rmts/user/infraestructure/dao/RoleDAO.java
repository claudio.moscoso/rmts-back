package cl.buildersoft.rmts.user.infraestructure.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import cl.buildersoft.rmts.user.domain.entity.RoleEntity;

public interface RoleDAO extends JpaRepository<RoleEntity, Integer> {
}
