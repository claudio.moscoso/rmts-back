package cl.buildersoft.rmts.user.domain.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import cl.buildersoft.rmts.product.domain.entity.ProductStatusEntity;

@Entity
@Table(name = "role_status")
public class RoleStatusEntity implements Serializable {
	private static final long serialVersionUID = -1126815367733272339L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@ManyToOne
	private RoleEntity role;

	@ManyToOne
	private ProductStatusEntity productStatus;

	public RoleStatusEntity() {
	}

	public RoleStatusEntity(Integer id, RoleEntity role, ProductStatusEntity productStatus) {
		super();
		this.id = id;
		this.role = role;
		this.productStatus = productStatus;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public RoleEntity getRole() {
		return role;
	}

	public void setRole(RoleEntity role) {
		this.role = role;
	}

	public ProductStatusEntity getProductStatus() {
		return productStatus;
	}

	public void setProductStatus(ProductStatusEntity productStatus) {
		this.productStatus = productStatus;
	}

	@Override
	public String toString() {
		return "RoleStatusEntity [id=" + id + ", role=" + role + ", productStatus=" + productStatus + "]";
	}

}
