#-------------------------------------
#--- BEGIN INITAL DATA ---------------
#-------------------------------------

INSERT INTO `user` (id, enabled, `password`, username) VALUES(1, 1, '$2a$10$UAMB1rXxF8W64Mk85KfZz.GT9YrgimAyKAVjFMG/F.H.RhZrZ1NWa', 'admin');
INSERT INTO `user` (id, enabled, `password`, username) VALUES(2, 1, '$2a$10$yBuUJFG.V6wJ6oHFeqiRVOWo39q.Oe7yBKQUfipuW3MWYSPfIFB3W', 'oper');
insert into `user` (id, enabled, `password`, username) values(3, 1, '$2a$10$R0gLGp8hqFtNagGA7Z.ZhesOH.bM9HmxZpX2Ws87k3e2l4TXFkbeK', 'tecn');
insert into `user` (id, enabled, `password`, username) values(4, 1, '$2a$10$HI0Ju4pkSJaOp3RX3vjELea6/6RHLiEErRZPm5WKm7l03azY9qtua', 'super');

INSERT INTO `role` (id, name) VALUES(1, 'ROLE_OPER');
INSERT INTO `role` (id, name) VALUES(2, 'ROLE_TECN');
INSERT INTO `role` (id, name) VALUES(3, 'ROLE_SUPE');
INSERT INTO `role` (id, name) VALUES(4, 'ROLE_DEVE');

insert into user_role(user_id, role_id) values(1, 1); #
insert into user_role(user_id, role_id) values(1, 2); #
insert into user_role(user_id, role_id) values(1, 3); #
insert into user_role(user_id, role_id) values(1, 4); #

insert into user_role(user_id, role_id) values(2, 1); #
insert into user_role(user_id, role_id) values(3, 2); #
insert into user_role(user_id, role_id) values(4, 3); #

INSERT INTO product_family(id, name) VALUES(1, 'Vehiculo');
INSERT INTO product_family(id, name) VALUES(2, 'Articuos de escritorio');

INSERT INTO product_type(id, name, product_family_id) VALUES(1, 'Moto', 1);
INSERT INTO product_type(id, name, product_family_id) VALUES(2, 'Camioneta', 1);
INSERT INTO product_type(id, name, product_family_id) VALUES(3, 'Camion', 1);
INSERT INTO product_type(id, name, product_family_id) VALUES(4, 'Sedan', 1);

INSERT INTO product_type(id, name, product_family_id) VALUES(5, 'Cuaderno', 2);
INSERT INTO product_type(id, name, product_family_id) VALUES(6, 'Lapiz', 2);
INSERT INTO product_type(id, name, product_family_id) VALUES(7, 'Pegamento', 2);

INSERT INTO product_status(id, access_key, name, initial, latest) VALUES(1, 'NEW', 'Nuevo', true, false);
INSERT INTO product_status(id, access_key, name, initial, latest) VALUES(2, 'FILLING', 'Completando', false, false);
INSERT INTO product_status(id, access_key, name, initial, latest) VALUES(3, 'REV-TECH', 'En Revision Especializada', false, false);
INSERT INTO product_status(id, access_key, name, initial, latest) VALUES(4, 'REV-LAW', 'En Revision Administrativa/Legal', false, false);
INSERT INTO product_status(id, access_key, name, initial, latest) VALUES(5, 'OPERATIVE', 'Operativo', false, false);
INSERT INTO product_status(id, access_key, name, initial, latest) VALUES(6, 'SOLD', 'Vendido', false, true);
INSERT INTO product_status(id, access_key, name, initial, latest) VALUES(7, 'UNUSABLE', 'Inutilizable', false, true);

INSERT INTO product_status(id, access_key, name, initial, latest) VALUES(8, 'WASHED', 'Lavado', false, false);
INSERT INTO product_status(id, access_key, name, initial, latest) VALUES(9, 'PHOTO', 'Fotografiando', false, true);
INSERT INTO product_status(id, access_key, name, initial, latest) VALUES(10, 'SAVED', 'Guardado', false, true);
INSERT INTO product_status(id, access_key, name, initial, latest) VALUES(11, 'LAW-PROBLEM', 'Problema Legal', false, false);


#Motos
INSERT INTO `field` (id, name, `type`, product_type_id) VALUES(1, 'Cilindrada', 'TEXT', 1);
INSERT INTO `field` (id, name, `type`, product_type_id) VALUES(2, 'Parabrisas', 'TEXT', 1);
INSERT INTO `field` (id, name, `type`, product_type_id) VALUES(3, 'Maletas', 'TEXT', 1);
INSERT INTO `field` (id, name, `type`, product_type_id) VALUES(4, 'Velocidades', 'TEXT', 1);
INSERT INTO `field` (id, name, `type`, product_type_id) VALUES(5, 'Pulgadas Neumaticos', 'TEXT', 1);
INSERT INTO `field` (id, name, `type`, product_type_id) VALUES(30, 'Enajenación Prohibida', 'TEXT', 1);
INSERT INTO `field` (id, name, `type`, product_type_id) VALUES(31, 'Prenda', 'TEXT', 1);
INSERT INTO `field` (id, name, `type`, product_type_id) VALUES(32, 'Orden de Embargo Prohibida', 'TEXT', 1);
#Camioneta
INSERT INTO `field` (id, name, `type`, product_type_id) VALUES(6, 'Doble tracción', 'TEXT', 2);
INSERT INTO `field` (id, name, `type`, product_type_id) VALUES(7, 'Cerrada/Abierta', 'TEXT', 2);
INSERT INTO `field` (id, name, `type`, product_type_id) VALUES(8, 'Asiendos', 'TEXT', 2);
INSERT INTO `field` (id, name, `type`, product_type_id) VALUES(9, 'Cabina', 'TEXT', 2);
INSERT INTO `field` (id, name, `type`, product_type_id) VALUES(10, 'Tara', 'TEXT', 2);
INSERT INTO `field` (id, name, `type`, product_type_id) VALUES(33, 'Enajenación Prohibida', 'TEXT', 2);
INSERT INTO `field` (id, name, `type`, product_type_id) VALUES(34, 'Prenda', 'TEXT', 2);
INSERT INTO `field` (id, name, `type`, product_type_id) VALUES(35, 'Orden de Embargo Prohibida', 'TEXT', 2);
#Camion
INSERT INTO `field` (id, name, `type`, product_type_id) VALUES(11, 'COE', 'TEXT', 3);
INSERT INTO `field` (id, name, `type`, product_type_id) VALUES(12, 'Longitud', 'TEXT', 3);
INSERT INTO `field` (id, name, `type`, product_type_id) VALUES(13, 'Cabina abatible', 'TEXT', 3);
INSERT INTO `field` (id, name, `type`, product_type_id) VALUES(14, 'Combustible', 'TEXT', 3);
INSERT INTO `field` (id, name, `type`, product_type_id) VALUES(15, 'Neumaticos', 'TEXT', 3);
INSERT INTO `field` (id, name, `type`, product_type_id) VALUES(36, 'Enajenación Prohibida', 'TEXT', 3);
INSERT INTO `field` (id, name, `type`, product_type_id) VALUES(37, 'Prenda', 'TEXT', 3);
INSERT INTO `field` (id, name, `type`, product_type_id) VALUES(38, 'Orden de Embargo Prohibida', 'TEXT', 3);
#Sedan
INSERT INTO `field` (id, name, `type`, product_type_id) VALUES(16, 'Puertas', 'TEXT', 4);
INSERT INTO `field` (id, name, `type`, product_type_id) VALUES(17, 'Maleta (ltrs)', 'TEXT', 4);
INSERT INTO `field` (id, name, `type`, product_type_id) VALUES(18, 'Velocidades', 'TEXT', 4);
INSERT INTO `field` (id, name, `type`, product_type_id) VALUES(19, 'Caja de cambios', 'TEXT', 4);
INSERT INTO `field` (id, name, `type`, product_type_id) VALUES(20, 'Cilindrada', 'TEXT', 4);
INSERT INTO `field` (id, name, `type`, product_type_id) VALUES(39, 'Enajenación Prohibida', 'TEXT', 4);
INSERT INTO `field` (id, name, `type`, product_type_id) VALUES(40, 'Prenda', 'TEXT', 4);
INSERT INTO `field` (id, name, `type`, product_type_id) VALUES(41, 'Orden de Embargo Prohibida', 'TEXT', 4);
#Cuaderno
INSERT INTO `field` (id, name, `type`, product_type_id) VALUES(21, '3 en 1', 'TEXT', 5);
INSERT INTO `field` (id, name, `type`, product_type_id) VALUES(22, 'Hojas', 'TEXT', 5);
INSERT INTO `field` (id, name, `type`, product_type_id) VALUES(23, 'Con espiral', 'TEXT', 5);
INSERT INTO `field` (id, name, `type`, product_type_id) VALUES(42, 'Robado', 'TEXT', 5);
#Lapiz
INSERT INTO `field` (id, name, `type`, product_type_id) VALUES(24, 'Color', 'TEXT', 6);
INSERT INTO `field` (id, name, `type`, product_type_id) VALUES(25, 'Tipo (pasta/grafito)', 'TEXT', 6);
INSERT INTO `field` (id, name, `type`, product_type_id) VALUES(26, 'Con goma', 'TEXT', 6);
INSERT INTO `field` (id, name, `type`, product_type_id) VALUES(43, 'Robado', 'TEXT', 6);
#Pegamento
INSERT INTO `field` (id, name, `type`, product_type_id) VALUES(27, 'Color', 'TEXT', 7);
INSERT INTO `field` (id, name, `type`, product_type_id) VALUES(28, 'ML', 'TEXT', 7);
INSERT INTO `field` (id, name, `type`, product_type_id) VALUES(29, 'Toxico', 'TEXT', 7);
INSERT INTO `field` (id, name, `type`, product_type_id) VALUES(44, 'Robado', 'TEXT', 7);


#INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id, readonly) VALUES();

#----------------------------------------------------------
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(1,2,1,3,1);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(2,2,1,3,2);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(3,2,1,3,3);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(4,2,1,3,4);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(5,2,1,3,5);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(6,3,1,4,30);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(7,3,1,4,31);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(8,3,1,4,32);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(9,4,1,3,1);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(10,4,1,3,2);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(11,4,1,3,3);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(12,4,1,3,4);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(13,4,1,3,5);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(14,4,1,4,30);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(15,4,1,4,31);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(16,4,1,4,32);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(17,2,2,3,6);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(18,2,2,3,7);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(19,2,2,3,8);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(20,2,2,3,9);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(21,2,2,3,10);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(22,3,2,4,33);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(23,3,2,4,34);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(24,3,2,4,35);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(25,4,2,3,6);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(26,4,2,3,7);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(27,4,2,3,8);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(28,4,2,3,9);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(29,4,2,3,10);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(30,4,2,4,33);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(31,4,2,4,34);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(32,4,2,4,35);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(33,2,3,3,1);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(34,2,3,3,2);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(35,2,3,3,3);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(36,2,3,3,4);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(37,2,3,3,5);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(38,3,3,4,30);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(39,3,3,4,31);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(40,3,3,4,32);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(41,4,3,3,1);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(42,4,3,3,2);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(43,4,3,3,3);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(44,4,3,3,4);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(45,4,3,3,5);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(46,4,3,4,30);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(47,4,3,4,31);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(48,4,3,4,32);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(49,2,4,3,1);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(50,2,4,3,2);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(51,2,4,3,3);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(52,2,4,3,4);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(53,2,4,3,5);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(54,3,4,4,30);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(55,3,4,4,31);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(56,3,4,4,32);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(57,4,4,3,1);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(58,4,4,3,2);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(59,4,4,3,3);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(60,4,4,3,4);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(61,4,4,3,5);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(62,4,4,4,30);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(63,4,4,4,31);
INSERT INTO role_type_status_field(id, role_id, product_type_id, product_status_id, field_id) VALUES(64,4,4,4,32);
#----------------------------------------------------------

INSERT INTO rmts.next_status(id, current_status_id, next_status_id) VALUES(1, 1, 2);
INSERT INTO rmts.next_status(id, current_status_id, next_status_id) VALUES(2, 1, 3);
INSERT INTO rmts.next_status(id, current_status_id, next_status_id) VALUES(3, 1, 5);
INSERT INTO rmts.next_status(id, current_status_id, next_status_id) VALUES(4, 1, 7);
INSERT INTO rmts.next_status(id, current_status_id, next_status_id) VALUES(5, 3, 4);
INSERT INTO rmts.next_status(id, current_status_id, next_status_id) VALUES(6, 3, 6);
INSERT INTO rmts.next_status(id, current_status_id, next_status_id) VALUES(7, 3, 11);

INSERT INTO rmts.next_status(id, current_status_id, next_status_id) VALUES(8, 5, 4);
INSERT INTO rmts.next_status(id, current_status_id, next_status_id) VALUES(9, 5, 6);
INSERT INTO rmts.next_status(id, current_status_id, next_status_id) VALUES(10, 5, 11);

INSERT INTO rmts.next_status(id, current_status_id, next_status_id) VALUES(11, 7, 4);
INSERT INTO rmts.next_status(id, current_status_id, next_status_id) VALUES(12, 7, 6);
INSERT INTO rmts.next_status(id, current_status_id, next_status_id) VALUES(13, 7, 11);

INSERT INTO rmts.next_status(id, current_status_id, next_status_id) VALUES(14, 2, 4);
INSERT INTO rmts.next_status(id, current_status_id, next_status_id) VALUES(15, 2, 6);
INSERT INTO rmts.next_status(id, current_status_id, next_status_id) VALUES(16, 2, 11);

INSERT INTO rmts.next_status(id, current_status_id, next_status_id) VALUES(17, 4, 8);
INSERT INTO rmts.next_status(id, current_status_id, next_status_id) VALUES(18, 4, 9);
INSERT INTO rmts.next_status(id, current_status_id, next_status_id) VALUES(19, 4, 10);

INSERT INTO rmts.next_status(id, current_status_id, next_status_id) VALUES(20, 6, 8);
INSERT INTO rmts.next_status(id, current_status_id, next_status_id) VALUES(21, 6, 9);
INSERT INTO rmts.next_status(id, current_status_id, next_status_id) VALUES(22, 6, 10);

INSERT INTO rmts.next_status(id, current_status_id, next_status_id) VALUES(23, 11, 8);
INSERT INTO rmts.next_status(id, current_status_id, next_status_id) VALUES(24, 11, 9);
INSERT INTO rmts.next_status(id, current_status_id, next_status_id) VALUES(25, 11, 10);

INSERT INTO rmts.next_status(id, current_status_id, next_status_id) VALUES(26, 4, 1);

INSERT INTO rmts.role_status(role_id, product_status_id) value(1,1);
INSERT INTO rmts.role_status(role_id, product_status_id) value(2,2);
INSERT INTO rmts.role_status(role_id, product_status_id) value(2,3);
INSERT INTO rmts.role_status(role_id, product_status_id) value(2,5);
INSERT INTO rmts.role_status(role_id, product_status_id) value(2,7);

INSERT INTO rmts.role_status(role_id, product_status_id) value(3,4);
INSERT INTO rmts.role_status(role_id, product_status_id) value(3,6);
INSERT INTO rmts.role_status(role_id, product_status_id) value(3,11);

INSERT INTO rmts.role_status(role_id, product_status_id) value(4,1);
INSERT INTO rmts.role_status(role_id, product_status_id) value(4,2);
INSERT INTO rmts.role_status(role_id, product_status_id) value(4,3);
INSERT INTO rmts.role_status(role_id, product_status_id) value(4,4);
INSERT INTO rmts.role_status(role_id, product_status_id) value(4,5);
INSERT INTO rmts.role_status(role_id, product_status_id) value(4,6);
INSERT INTO rmts.role_status(role_id, product_status_id) value(4,7);
INSERT INTO rmts.role_status(role_id, product_status_id) value(4,8);
INSERT INTO rmts.role_status(role_id, product_status_id) value(4,9);
INSERT INTO rmts.role_status(role_id, product_status_id) value(4,10);
INSERT INTO rmts.role_status(role_id, product_status_id) value(4,11);

#-------------------------------------
#--- END INITAL DATA -----------------
#-------------------------------------



